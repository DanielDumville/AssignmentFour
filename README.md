# AssignmentFour

Assignment four for Noroff and experis Academy, "Komputer Store".

Creation of a single page web application for a laptop store, using JavaScript and HTML.

Within the webpage it is possible to save up money to buy one of 5 laptops, whose data comes from a 
provided API. Users can save up money by working and banking that money to their balance.

The user may also take out a loan (if they do not already have a loan), if the loan is no greater than 
twice the amount of money they currently have in their bank balance.

Once the user has taken out a loan a button appears allowing the user to pay back the loan from their
pre-banked pay. If they choose not to pay back the loan then 10% of their pay to be banked goes towards
paying off their loan.

Author Daniel Dumville.

